package com.npci.customer_transaction_project.configuration;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
 
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter
{
    @Override
    protected void configure(HttpSecurity http) throws Exception 
    {
        http
         .csrf().disable()
         .authorizeRequests()
         .antMatchers("/api/v1/customer/allcustomers",
        		 "/api/v1/customer/getalltransactiondetails").hasRole("NORMAL")
         
         
         .antMatchers(
        		 "/api/v1/customer/getcustomerbyId/{id}",
        		 "/api/v1/customer/addorupdateCustomers",
        		 "/api/v1/customer/updateCustomerbyId/{id}",
        		 "/api/v1/customer/deletecustomerbyId/{id}",
        		 "/api/v1/customer/getcustomerbyage/{age}",
        		 "//api/v1/customer/getcustomerbyfirstname/{first_name}",
        		 "/api/v1/customer/gettransactionsbydate",
        		 "/api/v1/customer/addtransactions",
        		 "/api/v1/customer/gettransactiondetailsbyId/{id}").hasRole("ADMIN")
         .anyRequest().authenticated()
         .and()
         .httpBasic();
    }
  
    @Autowired
    public void configure(AuthenticationManagerBuilder auth) 
            throws Exception 
    {
        auth.inMemoryAuthentication()
          .withUser("ahmad").password(this.passwordEncoder().encode("ahmad")).roles("ADMIN");
        
        auth.inMemoryAuthentication()
        .withUser("Deepak").password(this.passwordEncoder().encode("Deepak@217")).roles("NORMAL");
    }
    
    @Bean
    public PasswordEncoder passwordEncoder() {
    	return new BCryptPasswordEncoder(10);
    }
}