package com.npci.customer_transaction_project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.npci.customer_transaction_project.entity.User;
import com.npci.customer_transaction_project.repository.UserRepository;

@RestController
@RequestMapping("/api/v1/auth")
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@GetMapping("/")
	public String home() {
		return "main";
	}

	@GetMapping("/user")
	public String user() {
		return ("<h1>Welcome User</h1>");
	}

	@GetMapping("/admin")
	public String admin() {
		return ("<h1>Welcome Admin</h1>");
	}

	@PostMapping("/addAuth")
	public ResponseEntity<User> addauthentication(@RequestBody User user) {
		User users = null;
		users = userRepository.save(user);
		return new ResponseEntity<User>(users, HttpStatus.OK);

	}
}