package com.npci.customer_transaction_project.dao1;

import java.util.List;

import com.npci.customer_transaction_project.entity.Transaction;

public interface TransactionDao {

	public List<Transaction> getTransactionByDate();
}
