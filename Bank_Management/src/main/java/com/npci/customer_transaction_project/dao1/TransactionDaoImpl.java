package com.npci.customer_transaction_project.dao1;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.npci.customer_transaction_project.dao.support.NameParametersJdbcDaoSupportClass;
import com.npci.customer_transaction_project.entity.Transaction;

@Repository
public class TransactionDaoImpl extends NameParametersJdbcDaoSupportClass implements TransactionDao {

	@Override
	public List<Transaction> getTransactionByDate() {

		List<Transaction> transaction = null;

		try {
			String query = "select * from transactions order by trans_date desc";
			transaction = getNamedParameterJdbcTemplate().getJdbcOperations().query(query,
					new BeanPropertyRowMapper<Transaction>(Transaction.class));

		} catch (Exception e) {
			e.getStackTrace();

		}
		return transaction;

	}

}
