package com.npci.customer_transaction_project.service;

import java.util.List; 

import org.springframework.stereotype.Service;
import com.npci.customer_transaction_project.entity.Transaction;

@Service
public interface TransactionService {
	public Transaction getTransactionsDetailsById(int transaction_Id);

	public List<Transaction> getAllTransactionDetails();

	public Transaction addTransaction(Transaction transaction) throws Exception;
	
	
	//This method is called from dao impl
	public List<Transaction> getTransactionByDate();

}
